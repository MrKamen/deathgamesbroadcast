package me.mrkamen.deathgamesbroadcast;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

public class DeathGamesListener implements Listener {
    private final DeathGamesBroadcast plugin;
    private long lastTime = System.currentTimeMillis();

    public DeathGamesListener(DeathGamesBroadcast plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPluginMessage(PluginMessageEvent ev) {
        if (!ev.getTag().equals("BungeeCord")) {
            return;
        }

        if (!(ev.getSender() instanceof Server)) {
            return;
        }
        ByteArrayInputStream stream = new ByteArrayInputStream(ev.getData());
        DataInputStream in = new DataInputStream(stream);
        try {
            String channel = in.readUTF();
            if (!channel.equals(plugin.getChannel())) {
                return;
            }
            String answer = in.readUTF();
            String[] data = answer.split(";");
            lastTime = System.currentTimeMillis();
            this.plugin.changeCache(data[0], Integer.parseInt(data[1]), Integer.parseInt(data[2]));
        } catch (IOException e) {
            this.plugin.getLogger().info("Не удалось расшифровать ответ от сервера");
            e.printStackTrace();
        }
    }

    public long getLastTime() {
        return lastTime;
    }
}
