package me.mrkamen.deathgamesbroadcast;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class DeathGamesCommands extends Command {
    private final DeathGamesBroadcast plugin;
    public DeathGamesCommands(String name, DeathGamesBroadcast plugin) {
        super(name);
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) return;
        if (!this.plugin.readyServer()) {
            sender.sendMessage(ChatColor.AQUA +  "[Смертельные Игры]" + ChatColor.RED + " Игра уже началась ");
            return;
        }
        ((ProxiedPlayer) sender).connect(this.plugin.getTargetServer());
    }
}
