package me.mrkamen.deathgamesbroadcast;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.hover.content.Text;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.plugin.Plugin;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public final class DeathGamesBroadcast extends Plugin {
    private String status = "default";
    private int actualOnline = 0;
    private int maxOnline = 0;
    private final String channel = "deathgames:broadcast";
    private DeathGamesListener deathGamesListener;
    private ServerInfo targetServer;

    @Override
    public void onEnable() {
        this.targetServer = ProxyServer.getInstance().getServerInfo("dg-1");
        if (this.targetServer == null) throw new IllegalArgumentException("Не удалось найти целевой сервер в конфиге BungeeCord");
        this.getProxy().registerChannel(channel);
        this.deathGamesListener = new DeathGamesListener(this);
        this.getProxy().getPluginManager().registerListener(this, this.deathGamesListener);
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new DeathGamesCommands("joindg", this));
        startSchedulerCache();
        startSchedulerBroadcast();
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    public boolean sendMessage(String message, ServerInfo server) {
        if (this.targetServer.getPlayers().size() < 1) return false;
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(stream);
        try {
            out.writeUTF(message);
        } catch (IOException e) {
            getLogger().info("Не удалось отправить сообщение на сервер " + server.getName() + ", проверьте его наличие в конфиге BungeeCord");
            e.printStackTrace();
        }

        server.sendData(channel, stream.toByteArray());
        return true;
    }

    public void startSchedulerCache() {
        ProxyServer.getInstance().getScheduler().schedule(this, () -> {
            if ((System.currentTimeMillis() - this.deathGamesListener.getLastTime())/1000 >= 3) {
                this.status = "START";
                this.actualOnline = 0;
                this.maxOnline = 1;
            }
            sendMessage("getInfo", this.targetServer);
        }, 0, 1, TimeUnit.SECONDS);
    }

    public void startSchedulerBroadcast() {
        Set<String> servers = new HashSet<>();
        servers.add("auth-1");
        servers.add("auth-2");
        servers.add("auth-3");
        servers.add("FixPlay");
        servers.add(this.targetServer.getName());
        String text = ChatColor.AQUA +  "[Смертельные Игры]"
                + ChatColor.WHITE + " Арена свободна для игры, "
                + ChatColor.GREEN + ChatColor.BOLD + "[ВХОД]"
                + ChatColor.GOLD + " <- нажми, чтобы войти";
        ComponentBuilder componentBuilder = new ComponentBuilder(text);
        ClickEvent clickEvent = new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/joindg");
        HoverEvent hoverEvent = new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text("Нажми, чтобы присоединиться"));
        componentBuilder.event(clickEvent);
        componentBuilder.event(hoverEvent);
        BaseComponent[] message = componentBuilder.create();
        ProxyServer.getInstance().getScheduler().schedule(this, () -> {
            if (!readyServer()) return;
            for (ProxiedPlayer player : getProxy().getPlayers()) {
                Server server = player.getServer();
                if (server == null) continue; // Да, действительно может быть null. Спасибо разработчикам
                if (servers.contains(server.getInfo().getName())) continue;
                player.sendMessage(message);
            }
        }, 15, 30, TimeUnit.SECONDS);
    }

    public boolean readyServer() {
        return this.status.equals("START") && this.actualOnline < this.maxOnline;
    }

    public String getChannel() {
        return this.channel;
    }

    public void changeCache(String status, int online, int maxOnline) {
        this.status = status;
        this.actualOnline = online;
        this.maxOnline = maxOnline;
    }

    public ServerInfo getTargetServer() {
        return this.targetServer;
    }
}
